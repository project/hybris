<?php

/**
 * @file
 * Simplified API call wrappers to work between other modules.
 */

function hybris_drush_command() {
  return array(
    'api_hybris_command' => array(
      'description' => dt('Runs an arbitrary Hybris OCC SDK command.'),
      'arguments' => array(
        'service' => 'The service in question (example: OCCAccount)',
        'action'  => 'The method to call (example: getAccount)',
      ),
      'options' => array(
        'uname' => array(
          'description' => 'User to authenticate as.',
        ),
        'pass' => array(
          'description' => 'Password for same user.',
        ),
        'token' => array(
          'description' => 'Use a previously gained authorization token'
        ),
        'params' => array(
          'description' => 'Service params, in url order.'
        ),
      ),
    ),
    'hybris_api_export_products' => array(
      'description' => dt('Usage: drush hybris_api_export_products --catalog='),
      'options' => array(
        'catalog' => array(
          'description' => 'Product catalog to get products from.'
        ),
        'version' => array(
          'description' => 'Catalog version. Must be provided along with catalog.'
        ),
        'modifiedTime' => array(
          'description' => 'When this parameter is set, only products modified after given time will be returned.This parameter should be in RFC-8601 format.'
        ),
        'currentPage' => array(
          'description' => 'The current result page requested.'
        ),
        'pageSize' => array(
          'description' => 'The number of results returned per page.'
        ),
        'fields' => array(
          'description' => 'Fields you want to return.'
        )
      )
    )
  );
}

function drush_hybris_api_hybris_command($service, $action) {
  if (!class_exists($service)) {
    return drush_set_error("No such service.");
  }

  $sp = _hybris_api_get_api_base_params();

  // Auth to the service.
  if (!$token = drush_get_option('token')) {
    $authService = new OCCAccount($sp['url'], $sp['site_id'], $sp['client_id'], $sp['secret'],
      $sp['accept_language'], $sp['catalog_id'], $sp['user_language'], $sp['currency']);

    $resp = $authService->auth(drush_get_option('uname'), drush_get_option('pass'));

    $token = $resp->access_token;
  }

  $service = new $service($sp['url'], $sp['site_id'], $sp['client_id'], $sp['secret'],
    $sp['accept_language'], $sp['catalog_id'], $sp['user_language'], $sp['currency']);

  if (!method_exists($service, $action)) {
    return drush_set_error("No such action.");
  }

  $options = drush_get_option('params', array());
  if (!is_array($options)) {
    $options = parse_str($options);
  }

  if (!empty($token)) {
    array_unshift($options, $token);
  }

  $result = call_user_func_array(array($service, $action), $options);

  drush_print_r($result);
}

function drush_hybris_api_export_products() {
  $catalog = drush_get_option('catalog');
  $version = drush_get_option('version');
  $modifiedTime = drush_get_option('modifiedTime');
  $currentPage = drush_get_option('currentPage');
  $pageSize = drush_get_option('pageSize');
  $fields = drush_get_option('fields');

  if (!$catalog || !$version) {
    drush_print("Missing required parameters: catalog, version");
    drush_print("Usage: drush hybris_api_export_products --catalog=<catalog_name> --version=<catalog_version> --modifiedTime=<e.g 2014-01-11T06:40:00-0700> --currentPage=0 --pageSize=10 --fields=<comma_separated_field_list>");
    return;
  }

  $params = array(
    'catalog' => $catalog,
    'version' => $version
  );

  if ($modifiedTime) {
    $params['timestamp'] = $modifiedTime;
  }
  if (!$currentPage) {
    $currentPage = 0;
  }
  if (!$pageSize) {
    $pageSize = 10;
  }
  if (!$fields) {
    $fields = "catalog,version,totalPageCount,currentPage,totalProductCount,products(code,price,creationTime,modifiedTime,stock,purchasable,hazmat,upc,discountedPrice,taxCode,max_order_qty,serviceType,variantOptions(code,priceData,stock))";
  }

  $params['currentPage'] = $currentPage;
  $params['pageSize'] = $pageSize;
  $params['fields'] = $fields;

  $language = variable_get('locale_code');

  $sp = array(
    'url'             => variable_get('hybris_api_url'),
    'site_id'         => variable_get('hybris_api_site_id'),
    'client_id'       => variable_get('hybris_api_client_id'),
    'secret'          => variable_get('hybris_api_client_secret'),
    'accept_language' => $language,
    'catalog_id'      => variable_get('hybris_api_client_catalog_id'),
    'user_language'   => $language,
    'currency'        => variable_get('currency_code'),
    'debug'           => variable_get('hybris_api_debug')
  );

  $prodApi = new OCCProduct($sp['url'], $sp['site_id'], $sp['client_id'], $sp['secret'],
    $sp['accept_language'], $sp['catalog_id'], $sp['user_language'], $sp['currency']);

  $result = $prodApi->exportProducts($params);

  drush_print_r($result);
}
