<?php

/**
 * @file
 * Adds support for Product integration with Hybris from Drupal.
 */

require_once 'OCCBase.inc';

class OCCProduct extends OCCBase {

  protected $user_language;
  protected $currency;

  public function __construct($url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $user_language, $currency, $point_of_service, $debug = false) {

    parent::__construct($url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $point_of_service, $debug);

    $this->user_language = $user_language;
    $this->currency = $currency;
  }

  /**
  * Product export
  *
  * @param  string $params['catalog'] - required
  * @param  string $params['version'] - required
  * @param  string $params['timestamp'] - ISO-8601 (e.g. 2014-09-11T06:40:00-0700)
  * @param  string $params['currentPage']
  * @param  string $params['pageSize']
  * @param  string $params['fields']
  *
  * @return stdClass {code, result}
  */
  public function exportProducts(array $params = null) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restGet("/export/products", $auth, $params);
  }

  /**
  * Exports VKB kits.
  *
  * @return stdClass {code, result}
  */
  public function exportKits() {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $params = array();
    return $this->restGet("/export/kits", $auth, $params);
  }

  /**
  * Get product stock level.
  *
  * @param string $code
  *
  * @return stdClass {code, result}
  */
  public function getProductStockLevel($code) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restGet("/products/{$code}/stock/{$this->point_of_service}", $auth);
  }
}
