<?php

/**
 * @file
 * Adds support for Account integration with Hybris from Drupal.
 */

require_once 'OCCBase.inc';

class OCCAccount extends OCCBase {

  protected $user_language;
  protected $currency;


  /**
   * Constructor
   *
   * @param $url - Required
   * Hybris API url, e.g. https://localhost:9002
   *
   * @param $site_id - Required
   * baseSiteId in Hybris, e.g. usa_site.
   *
   * @param $client_id - Required
   * The client that accesses the API, provided by Hybris.
   *
   * @param $secret - Required
   * Secret password for $client_id above.
   *
   * @param $accept_language - Required
   *
   * @param $catalog_id - Required
   * Product catalog provided by Hybris.
   *
   * @param $user_language - Required (e.g. en-US).
   *
   * @param $currency - Required (e.g. USD).
   *
   */
  public function __construct($url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $user_language, $currency, $point_of_service, $debug = false) {
    parent::__construct($url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $point_of_service, $debug);

    $this->user_language = $user_language;
    $this->currency = $currency;
  }

  /**
   * Authenticate user
   *
   * @param $username - required (user's email)
   * @param $password - required
   *
   * @return stdClass {code, result}
   */
  public function auth($username, $password) {
    $data = array(
      'grant_type' => 'password',
      'username' => strtolower($username),
      'password' => $password,
      'client_id' => $this->client_id,
      'client_secret' => $this->secret
    );
    return $this->restPPP(parent::OCC_TOKEN_BASEPATH, array(), $data);
  }

  /**
   * Refresh a user's access token. Use this when access_token is expired or lost.
   *
   * @param $refreshToken - required
   *
   * @return stdClass {code, result}
   */
  public function refreshAccessToken($refreshToken) {
    $data = array(
      'grant_type' => 'refresh_token',
      'refresh_token' => $refreshToken,
      'client_id' => $this->client_id,
      'client_secret' => $this->secret
    );
    return $this->restPPP(parent::OCC_TOKEN_BASEPATH, array(), $data);
  }

  /**
   * Register user. This does these in the order: createAccount(), auth(), and getAccount().
   *
   * @param $firstName - required
   * @param $lastName - required
   * @param $email - required
   * @param $password - required
   * @param $country - required (ISO code)
   * @param $receiveNewsletter - true|false
   * @param $language - Optional. This will override the constructor's language if passed in.
   *
   * @return
   * - stdClass {code, auth, profile} if successful.
   * - stdClass {code, result} if failed.
   */
  public function register($firstName, $lastName, $email, $country, $password, $receiveNewsletter, $language = null) {
    // Create account
    $create = $this->createAccount($firstName, $lastName, $email, $country, $password, $receiveNewsletter, $language);
    if ($create->code == 201) {
      // Login
      $auth = $this->auth($email, $password);
      if (isset($auth) && isset($auth->result) && isset($auth->result->access_token)) {
        // Get profile
        $profile = $this->getAccount($auth->result->access_token);
        $retObj = new stdClass();
        $retObj->auth = $auth;
        $retObj->profile = $profile;
        $retObj->code = $profile->code;
        return $retObj;
      } else {
        // Failed to login
        return $auth;
      }
    } else {
      // Failed to create account
      return $create;
    }
  }

  /**
   * Wrapper to log in and get back profile information.
   *
   * @param string $username
   *  User's email address.
   * @param string $password
   *  Password.
   *
   * @return
   * - stdClass {code, auth, profile} if successful.
   * - stdClass {code, result} if failed.
   */
  public function login($username, $password) {
    $auth = $this->auth($username, $password);
    if (isset($auth->result->access_token)) {
      $profile = $this->getAccount($auth->result->access_token);

      $retObj = new stdClass();
      $retObj->auth = $auth;
      $retObj->profile = $profile;
      $retObj->code = $profile->code;
      return $retObj;
    }
    return $auth;
  }

  /**
   * Create a new user account.
   *
   * @param $firstName - required
   * @param $lastName - required
   * @param $email - required
   * @param $password - required
   * @param $country - required (ISO code)
   * @param $receiveNewsletter - true|false
   * @param $language - Optional. This will override the constructor's language if passed in.
   *
   * @return stdClass {code, result}
   */
  public function createAccount($firstName, $lastName, $email, $country, $password, $receiveNewsletter, $language = null) {
    $data = array(
      'login' => strtolower($email),
      'password' => $password,
      'firstName' => $firstName,
      'lastName' => $lastName,
      'country' => $country,
      'language' => ($language !== null ? $language : $this->user_language),
      'currency' => $this->currency,
      'receiveNewsletter' => $receiveNewsletter
    );
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restPPP('/users/', $auth, $data);
  }

  /**
   * Update user account.
   *
   * Note: This is a PATCH. If an optional param is null, it won't be updated and the current value stays the same.
   *
   * @param $userToken - required
   * @param $firstName - required
   * @param $lastName - required
   * @param $birthDate (mm-dd) - set to blank '' to remove.
   * @param $country (ISO)
   * @param $receiveNewsletter (expect 'true' or 'false' as string)
   *
   * @return stdClass {code, result}
   */
  public function updateAccount($userToken, $firstName, $lastName, $birthDate = null, $country = null, $receiveNewsletter = null) {
    $data = array(
      'firstName' => $firstName,
      'lastName' => $lastName
    );
    if ($birthDate !== null) $data['birthDate'] = $birthDate;
    if ($country !== null) $data['country'] = $country;

    // To reset birthDate, set it to blank '' or 'resetBirthdate'
    if ($data['birthDate'] === '') {
      $data['birthDate'] = 'resetBirthdate';
    }

    // making sure it's string 'true' or 'false'
    if ($receiveNewsletter === true || strcasecmp($receiveNewsletter, 'true') == 0) $data['receiveNewsletter'] = 'true';
    else if ($receiveNewsletter === false || strcasecmp($receiveNewsletter, 'false') == 0) $data['receiveNewsletter'] = 'false';
    else {}

    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restPPP('/users/current', $auth, $data, 'PATCH');
  }

  /**
   * Customers update their email. Note that email is unique and is used to login.
   *
   * @param string $userToken
   * @param string $newEmail
   * @param string $password - current password
   *
   * @return stdClass {code, result}
   */
  public function updateEmail($userToken, $newEmail, $password) {
    $data = array(
      'newLogin' => strtolower($newEmail),
      'password' => $password
    );
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restPPP('/users/current/login', $auth, $data, 'PUT');
  }

  /**
   * Customers update their email. Note that email is unique and is used to login.
   *
   * @param string $userToken
   * @param string $newEmail
   * @param string $password - current password
   *
   * @return stdClass {code, result}
   */
  public function getAccount($userToken) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restGet('/users/current', $auth);
  }

  /**
   * Users update their password.
   *
   * @param string $userToken
   * @param string $old - current password
   * @param string $new - new password
   *
   * @return stdClass {code, result}
   */
  public function updatePassword($userToken, $old, $new) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    $data = array(
      'old' => $old,
      'new' => $new
    );
    return $this->restPPP('/users/current/password', $auth, $data, 'PUT');
  }

  /**
   * Change user password using the super system token.
   *
   * @param string $userId
   * @param string $new
   *
   * @return stdClass {code, result}
   */
  public function changeUserPassword($userId, $new) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $data = array(
      'new' => $new
    );
    return $this->restPPP('/users/'.$userId.'/password', $auth, $data, 'PUT');
  }

  /**
   * This sends out an email with a reset password link.
   * User would click on that link to go to the Reset Password page.
   *
   * @param string $email
   *
   * @return stdClass {code, result}
   */
  public function resetPassword($email) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restGet('/users/'.strtolower($email).'/resetpassword', $auth);
  }

  /**
   * Reset user's password from the "Forgot Password" process.
   *
   * @param string $email
   * @param string $resetPasswordToken
   * @param string $newPassword
   *
   * @return stdClass {code, result}
   */
  public function updateForgotPassword($email, $resetPasswordToken, $newPassword) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $data = array(
      'token' => $resetPasswordToken,
      'new' => $newPassword
    );
    return $this->restPPP('/users/'.strtolower($email).'/password', $auth, $data, 'PUT');
  }

  /**
   * Get user preferences
   *
   * @param string $userToken - required
   *
   * @return stdClass {code, result}
   */
  public function getPrefs($userToken) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restGet('/users/current/prefs', $auth);
  }

  /**
   * Get user address(es)
   *
   * @param string $userToken - required
   * @param $addressId
   *
   * @return stdClass {code, result}
   */
  public function getAddress($userToken, $addressId = null) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    if ($addressId) {
      $resp = $this->restGet('/users/current/addresses/' . $addressId, $auth);
    } else {
      $resp = $this->restGet('/users/current/addresses/', $auth);
    }
    return $resp;
  }

  /**
   * Add user address
   *
   * @param string $userToken - required
   * @param $addressData - required
   *
   * @return stdClass {code, result}
   */
  public function addAddress($userToken, $addressData) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restPPP('/users/current/addresses/', $auth, $addressData);
  }

  /**
   * Delete user address
   *
   * @param string $userToken - required
   * @param $addressId - required
   *
   * @return stdClass {code, result}
   */
  public function deleteAddress($userToken, $addressId) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restDelete('/users/current/addresses/' . $addressId, $auth);
  }

  /**
   * Update user address
   *
   * @param string $userToken - required
   * @param $addressId - required
   * @param array $addressData - required
   *
   * @return stdClass {code, result}
   */
  public function updateAddress($userToken, $addressId, array $addressData) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restPPP('/users/current/addresses/' . $addressId, $auth, $addressData, 'PATCH');
  }

  /**
   * Get user profile.
   *
   * @param $userId
   * @param $userToken - This will override $userId param.
   *
   * @return stdClass {code, result}
   */
  public function getUserProfile($userId, $userToken = null) {
    if (!empty($userToken)) {
      $userId = 'current';
    } else {
      $userToken = $this->getClientAccessToken();
    }
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    return $this->restGet('/users/' . $userId, $auth);
  }

}
