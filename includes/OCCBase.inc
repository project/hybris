<?php

/**
 * @file
 * Core integration class for Drupal and Hybris which all others extend.
 */

class OCCBase {

  const OCC_TOKEN_BASEPATH = '/oauth/token';
  const OCC_API_BASEPATH = '/';
  const OCC_API_RESPONSE_FORMAT = 'application/json';
  const CURL_ERROR_HTTP_CODE = 500;
  const CURL_ERROR_TYPE = 'HybrisServerError';
  const CURL_ERROR_MESSAGE = 'Hybris Server Error';
  const WATCHDOG_TYPE = 'hybris_api';

  protected $client;
  protected $api_url;
  protected $site_id;
  protected $secret;
  protected $accept_language;
  protected $catalog_id;
  protected $client_id;
  protected $point_of_service;
  protected $debug;
  static $access_token = null;

  public function __construct($api_url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $point_of_service, $debug = false) {
    $this->api_url = $api_url;
    $this->site_id = $site_id;
    $this->secret = $secret;
    $this->accept_language = $accept_language;
    $this->catalog_id = $catalog_id;
    $this->client_id = $client_id;
    $this->point_of_service = $point_of_service;
    $this->debug = $debug;
  }

  /**
   * Get client access token. This is the 'super' token to be used for system resources like
   * user registration, product export, etc.
   *
   * @return token string
   */
  protected function getClientAccessToken() {
    if (!empty(SELF::$access_token)) {
      return SELF::$access_token;
    }

    $data = array(
      'grant_type' => 'client_credentials',
      'client_id' => $this->client_id,
      'client_secret' => $this->secret
    );

    $uri = $this->api_url . self::OCC_TOKEN_BASEPATH;

    $ch = curl_init($uri);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    $this->setCurlOptions($ch);
    curl_setopt($ch, CURLOPT_POSTFIELDS, (is_array($data) ? http_build_query($data) : $data));

    $result = json_decode(curl_exec($ch));
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($code == 200 && isset($result->access_token)) {
      SELF::$access_token = $result->access_token;
      return $result->access_token;
    }
  }

  /**
   * Rest GET
   *
   */
  protected function restGet($path, array $headers, array $query = null) {
    if (!$this->isAwake()) {
      $resp = new stdClass();
      $resp->code = $code;
      $resp->result = $this->createErrorResponse(self::CURL_ERROR_TYPE, self::CURL_ERROR_MESSAGE);
      return $resp;
    }
    if (!is_array($query)) $query = array();

    // additional headers
    $headers = array_merge($headers, $this->getCommonHeaders());

    // Oauth path does not contain site/store parts.
    if (self::OCC_TOKEN_BASEPATH != $path) {
      $path = $this->getFullPath($path);
    }
    $uri = $this->api_url . $path . "?" . http_build_query($query);

    $ch = curl_init($uri);
    $this->setCurlOptions($ch);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = json_decode(curl_exec($ch));
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // cURL error
    if (curl_error($ch)) {
      $code = self::CURL_ERROR_HTTP_CODE;
      $result = $this->createErrorResponse(self::CURL_ERROR_TYPE, self::CURL_ERROR_MESSAGE);
    }
    curl_close($ch);

    // Return stdClass object
    $resp = new stdClass();
    $resp->code = $code;
    $resp->result = $result;

    // Send to watchdog if not a 2xx response
    if ($code > 299 || $code < 200) {
      $request = new stdClass();
      $request->endpoint = $uri;
      $request->method = 'GET';
      $request->headers = $headers;
      $request->query = $query;
      $request->current_path = current_path();
      $this->logError($request, $resp);
    }

    return $resp;
  }

  /**
   * Rest POST, PUT, PATCH
   *
   */
  protected function restPPP($path, array $headers, $data, $method = 'POST', array $query = null) {
    if (!$this->isAwake()) {
      $resp = new stdClass();
      $resp->code = $code;
      $resp->result = $this->createErrorResponse(self::CURL_ERROR_TYPE, self::CURL_ERROR_MESSAGE);
      return $resp;
    }
    if (!is_array($query)) $query = array();

    // additional headers
    $headers = array_merge($headers, $this->getCommonHeaders());

    // Oauth path does not contain site/store parts.
    if (self::OCC_TOKEN_BASEPATH != $path) {
      $path = $this->getFullPath($path);
    }
    $uri = $this->api_url . $path . "?" . http_build_query($query);

    $ch = curl_init($uri);

    $this->setCurlOptions($ch);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_POSTFIELDS, (is_array($data) ? http_build_query($data) : $data));

    $result = json_decode(curl_exec($ch));
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // cURL error
    if (curl_error($ch)) {
      $code = self::CURL_ERROR_HTTP_CODE;
      $result = $this->createErrorResponse(self::CURL_ERROR_TYPE, self::CURL_ERROR_MESSAGE);
    }
    curl_close($ch);

    // Return stdClass object
    $resp = new stdClass();
    $resp->code = $code;
    $resp->result = $result;

    // Send to watchdog if not a 2xx response
    if ($code > 299 || $code < 200) {
      $request = new stdClass();
      $request->endpoint = $uri;
      $request->method = $method;
      $request->headers = $headers;
      $request->query = $query;
      $request->data = $data;
      $request->current_path = current_path();
      $this->logError($request, $resp);
    }

    return $resp;
  }

  /**
   * Rest DELETE
   *
   */
  protected function restDelete($path, array $headers) {
    if (!$this->isAwake()) {
      $resp = new stdClass();
      $resp->code = $code;
      $resp->result = $this->createErrorResponse(self::CURL_ERROR_TYPE, self::CURL_ERROR_MESSAGE);
      return $resp;
    }
    // additional headers
    $headers = array_merge($headers, $this->getCommonHeaders());

    $uri = $this->api_url . $this->getFullPath($path);

    $ch = curl_init($uri);

    $this->setCurlOptions($ch);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

    $result = json_decode(curl_exec($ch));
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // cURL error
    if (curl_error($ch)) {
      $code = self::CURL_ERROR_HTTP_CODE;
      $result = $this->createErrorResponse(self::CURL_ERROR_TYPE, self::CURL_ERROR_MESSAGE);
    }
    curl_close($ch);

    // Return stdClass object
    $resp = new stdClass();
    $resp->code = $code;
    $resp->result = $result;

    // Send to watchdog if not a 2xx response
    if ($code > 299 || $code < 200) {
      $request = new stdClass();
      $request->endpoint = $uri;
      $request->method = 'DELETE';
      $request->headers = $headers;
      $request->current_path = current_path();
      $this->logError($request, $resp);
    }

    return $resp;
  }

  /**
   * Create an error response that has the same format as the built-in Hybris API response.
   *
   * @param $type
   * @param $message
   *
   * @return stdClass
   */
  public function createErrorResponse($type, $message) {
    // If the API this code is triggered it usually means the API is down.
    // If so, wait for x minutes before making new requests.
    if ($this->isAwake()) {
      $sleep_interval = variable_get('hybris_api_sleep_interval', 60 * 5);
      variable_set('hybris_api_sleep_until', REQUEST_TIME + $sleep_interval);
    }
    $errorMsg = new stdClass();
    $errorMsg->type = $type;
    $errorMsg->message = $message;
    $error = new stdClass();
    $error->errors = array($errorMsg);
    return $error;
  }


  /**
   * Verify that the API hasn't been disabled. If it has, return an error
   * response.
   */
  public function isAwake() {
    $sleep_until = variable_get('hybris_api_sleep_until', 0);
    if (REQUEST_TIME > $sleep_until) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Contructs the full API endpoint.
   *
   * @param string @path
   */
  protected function getFullPath($path) {
    return self::OCC_API_BASEPATH . '/' . $this->site_id . '/' . ltrim($path, '/');
  }

  /**
   * Sets the necessary cURL options.
   */
  protected function setCURLOptions($ch) {
    $options = array (
      CURLOPT_CONNECTTIMEOUT => 10,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_HEADER => false,
      CURLOPT_SSL_VERIFYPEER => 0, // secure?
      CURLOPT_SSL_VERIFYHOST => 0,
      CURLOPT_HTTPGET => 1 // GET by default
    );
    curl_setopt_array($ch, $options);
  }

  /**
   * Logs errors to watchdog.
   *
   * @param object $request - API request object
   * @param object $response - API response object
   */
  protected function logError($request, $response) {
    // If debug = true then logs everything in request and response,
    // otherwise logs only API method, endpoint, and subject/reason/message in the response.
    if ($this->debug) {
      $msg = "BEGIN HYBRIS API EXCEPTION >>>>>>\n";
      $msg .= "--- REQUEST:\n".print_r($request, true)."\n--- RESPONSE:\n".print_r($response, true)."\n";
      $msg .= "<<<<<< END HYBRIS API EXCEPTION\n";
    } else {
      $errors = array();
      if (!empty($response->result->errors)) {
        foreach ($response->result->errors as $error) {
          if (!empty($error->subject) && !empty($error->reason)) {
            $errors[] = $error->subject . ': ' . $error->reason;
          } else {
            $errors[] = !empty($error->message) ? $error->message : $error->type;
          }
        }
      }
      $msg = $request->method . " " . $request->endpoint . " => " . implode("; ", $errors) . "\n";
    }

    if (function_exists('watchdog')) {
      watchdog(self::WATCHDOG_TYPE, '@msg', array('@msg' => $msg), WATCHDOG_ERROR);
    }

    if ($this->debug && function_exists('dd')) {
      dd($msg, self::WATCHDOG_TYPE);
    }
  }

  /**
   * Returns common REST headers.
   *
   * @return array
   */
  protected function getCommonHeaders() {
    return array(
      'Accept-language: ' . $this->accept_language,
      'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
    );
  }
}
