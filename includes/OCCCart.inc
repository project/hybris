<?php

/**
 * @file
 * Adds support for Cart integration with Hybris from Drupal.
 */

require_once 'OCCBase.inc';

class OCCCart extends OCCBase {

  const ANONYMOUS_USER = 'anonymous';

  protected $user_language;
  protected $currency;

  public function __construct($url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $user_language, $currency, $point_of_service, $debug = false) {

    parent::__construct($url, $site_id, $client_id, $secret, $accept_language, $catalog_id, $point_of_service, $debug);

    $this->user_language = $user_language;
    $this->currency = $currency;
  }

  /**
   * Returns the cart-related API authorization header and userId.
   *
   * @return array [header, userId]
   */
  private function getApiAuth($userToken = NULL) {
    return array(
      'header' => array(
        'Authorization: Bearer ' . (!empty($userToken) ? $userToken: $this->getClientAccessToken())
      ),
      'userId' => (!empty($userToken) ? 'current' : self::ANONYMOUS_USER)
    );
  }

  /**
   * Get cart(s)
   *
   * @param string $userToken - null for 'anonymous' user
   * @param string $cartId - if null then return all carts
   *
   * @return stdClass {code, result}
   */
  public function getCart($userToken = null, $cartId = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restGet('/users/'.$apiAuth['userId'].'/carts/'.$cartId, $apiAuth['header']);
  }

  /**
   * Delete cart
   *
   * @param $cartId - required
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function deleteCart($cartId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restDelete('/users/'.$apiAuth['userId'].'/carts/'.$cartId, $apiAuth['header']);
  }

  /**
   * Create cart
   *
   * @param $userToken - null for 'anonymous' user
   * @param $oldCartId - anonymous cart GUID
   * @param $toMergeCartGuid - user's cart GUID to merge anonymous cart to
   *
   * @return stdClass {code, result}
   */
  public function createCart($userToken = null, $oldCartId = null, $toMergeCartGuid = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'oldCartId' => $oldCartId,
      'toMergeCartGuid' => $toMergeCartGuid
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts', $apiAuth['header'], $data);
  }

  /**
   * Add item to cart. This will add to the existing qty if item already exists in cart.
   *
   * @param $cartId - required
   * @param $sku - required
   * @param $qty - required (must be > 0)
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function addItem($cartId, $sku, $qty, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'code' => $sku,
      'qty' => $qty
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/entries', $apiAuth['header'], $data);
  }

  /**
   * Update item quantity
   *
   * @param $cartId - required
   * @param $entryNumber - required (zero-based numbering)
   * @param $qty - required (must be > 0)
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function updateItem($cartId, $entryNumber, $qty, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'qty' => $qty
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/entries/'.$entryNumber, $apiAuth['header'], $data, 'PATCH');
  }

  /**
   * Delete item
   *
   * @param $cartId - required
   * @param $entryNumber - required (zero-based numbering)
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function deleteItem($cartId, $entryNumber, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restDelete('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/entries/'.$entryNumber, $apiAuth['header']);
  }

  /**
   * Get all supported delivery countries for the current store. The list is sorted alphabetically.
   *
   * @param $language
   *
   * @return stdClass {code, result}
   */
  public function getDeliveryCountries($language = 'en-US') {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $query = array(
      'lang' => $language
    );
    return $this->restGet('/deliverycountries', $auth, $query);
  }

  /**
   * Get all supported billing countries for the current store. The list is sorted alphabetically.
   *
   * @return stdClass {code, result}
   */
  public function getBillingCountries() {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restGet('/deliverycountries', $auth, array('billing' => true));
  }

  /**
   * Get all delivery modes supported for the current base store and cart delivery address.
   * There must be a delivery address for the cart, otherwise an empty list will be returned.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $userToken - null for anonymous user
   *
   * @return stdClass {code, result}
   */
  public function getAvailableDeliveryModes($cartId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restGet('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/deliverymodes', $apiAuth['header']);
  }

  /**
   * Sets a delivery address for the cart. The address country must be in current's base store delivery countries.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $addressId
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function setDeliveryAddress($cartId, $addressId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'addressId' => $addressId
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/addresses/delivery', $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Creates address and assigns it to the cart as delivery address.
   *
   * @param $userId  - 'anonymous' or registered user's email address.
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $addressData
   *
   * @return stdClass {code, result}
   */
  public function createDeliveryAddress($userId, $cartId, $addressData) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restPPP('/users/'.$userId.'/carts/'.$cartId.'/addresses/delivery', $auth, $addressData);
  }

  /**
   * Sets delivery mode with given identifier for the cart.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $deliveryModeId
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function setDeliveryMode($cartId, $deliveryModeId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'deliveryModeId' => $deliveryModeId
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/deliverymode', $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Deletes delivery mode from the cart.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function deleteDeliveryMode($cartId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restDelete('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/deliverymode', $apiAuth['header']);
  }


  /**
   * Sets cart payment mode.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $paymentMode - creditcard, debitcard, paypal, etc.
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function setPaymentMode($cartId, $paymentMode, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'paymentMode' => $paymentMode
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/paymentmode', $apiAuth['header'], $data);
  }

  /**
   * Creates an order.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $authorizedAmount - Provided by payment gateway
   * @param $transactionId - Provided by payment gateway
   * @param $paymentToken - Provided by payment gateway
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function createOrder($cartId, $authorizedAmount, $transactionId, $paymentToken, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'cartId' => $cartId,
      'authorizedAmount' => $authorizedAmount,
      'transactionId' => $transactionId,
      'paymentToken' => $paymentToken,
      'paymentStatus' => 'PAID'
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/orders/', $apiAuth['header'], $data);
  }

  /**
   * Gets single order details.
   *
   * @param $code - order code/guid
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function getOrderDetails($code, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restGet('/users/'.$apiAuth['userId'].'/orders/'.$code, $apiAuth['header']);
  }

  /**
   * Gets historical orders.
   *
   * @param $userToken
   * @param $count - the last $count orders
   *
   * @return stdClass {code, result}
   */
  public function getHistoricalOrders($userToken, $count = 10, $page = 0) {
    $auth = array(
      'Authorization: Bearer ' . $userToken
    );
    $query = array(
      'sort' => 'byDate',
      'statuses' => null,
      'currentPage' => $page,
      'pageSize' => $count
    );
    return $this->restGet('/users/current/orders/', $auth, $query);
  }

  /**
   * Adds VKB kit to cart.
   *
   * @param $cartId
   * @param $kitId
   * @param $skus
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function addKit($cartId, $kitId, $skus, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'kitId' => $kitId,
      'catalog' => $this->catalog_id,
      'productId' => $skus
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/kits', $apiAuth['header'], $data);
  }

  /**
   * Updates VKB entry in cart.
   *
   * @param $cartId
   * @param $kitId
   * @param $skus
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function updateKit($cartId, $kitId, $skus, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'catalog' => $this->catalog_id,
      'productId' => $skus
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/kits/'.$kitId, $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Deletes VKB kit from cart.
   *
   * @param $cartId
   * @param $kitId
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function deleteKit($cartId, $kitId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restDelete('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/kits/'.$kitId, $apiAuth['header']);
  }

  /**
   * Adds voucher to cart.
   *
   * @param $cartId
   * @param $voucherCode
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function addVoucher($cartId, $voucherCode, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'voucherId' => strtoupper(trim($voucherCode))
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/vouchers', $apiAuth['header'], $data);
  }

  /**
   * Deletes voucher from cart.
   *
   * @param $cartId
   * @param $voucherCode
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function deleteVoucher($cartId, $voucherCode, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restDelete('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/vouchers/'.strtoupper($voucherCode), $apiAuth['header']);
  }

  /**
   * Gets list of vouchers applied to the cart.
   *
   * @param $cartId
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function getVouchers($cartId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    return $this->restGet('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/vouchers/', $apiAuth['header']);
  }

  /**
   * Gets user's order details using the system access token.
   *
   * @param $userId  - 'anonymous' or registered user's email address.
   * @param $orderCode - uuid for anonymous order, code for authenticated order.
   *
   * @return stdClass {code, result}
   */
  public function getUserOrderDetails($userId, $orderCode) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restGet('/users/'.strtolower($userId).'/orders/'.$orderCode, $auth);
  }

  /**
   * Gets user historical orders using the system token.
   *
   * @param $userId  - 'anonymous' or registered user's email address.
   * @param $count
   *
   * @return stdClass {code, result}
   */
  public function getUserHistoricalOrders($userId, $count = 10) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $query = array(
      'sort' => 'byDate',
      'statuses' => null,
      'currentPage' => 0,
      'pageSize' => $count
    );
    return $this->restGet('/users/'.strtolower($userId).'/orders/', $auth, $query);
  }

  /**
   * Cancels an order.
   *
   * @param $userId  - 'anonymous' or registered user's email address.
   * @param $orderCode - uuid for anonymous order, code for authenticated order.
   * @param $consignmentId
   * @param $anonymousAuthentication - Anonymous needs to verify the email they used to place the order.
   *
   * @return stdClass {code, result}
   */
  public function cancelOrder($userId, $orderCode, $consignmentId, $anonymousAuthentication = null) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $data = array(
      'code' => $orderCode,
      'consignmentId' => $consignmentId,
      'userId' => $userId,
      'orderStatus' => 'CANCELLATION_REQUESTED'
    );
    if (!empty($anonymousAuthentication)) {
      $data['authentication'] = $anonymousAuthentication;
    }
    return $this->restPPP('/users/'.strtolower($userId).'/orders/'.$orderCode.'/consignments/'.$consignmentId.'/status', $auth, $data, 'PUT');
  }

  /**
   * Get delivery options for the current store based on shipping country.
   *
   * @param $countryIso
   *
   * @return stdClass {code, result}
   */
  public function getDeliveryModes($countryIso) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $query = array(
      'country' => $countryIso,
      'lang' => $this->accept_language
    );
    return $this->restGet('/deliverymodes', $auth, $query);
  }

  /**
   * Sets giftWrap to a cart entry.
   *
   * @param $cartId
   * @param $entryNumber
   * @param $giftWrapped
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function setGiftWrap($cartId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array();
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/giftWrap', $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Adds gift wrap message to cart.
   *
   * @param $cartId
   * @param $giftMessage
   * @param $userToken
   *
   * @return stdClass {code, result}
   */
  public function addGiftMessage($cartId, $giftMessage, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'giftWrapMessage' => $giftMessage
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/giftWrapMessage', $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Sets a billing address for the cart.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $addressId
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function setBillingAddress($cartId, $addressId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'addressId' => $addressId
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/addresses/billing', $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Creates address and assigns it to the cart as billing address.
   *
   * @param $userId  - 'anonymous' or registered user's email address.
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $addressData
   *
   * @return stdClass {code, result}
   */
  public function createBillingAddress($userId, $cartId, $addressData) {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    return $this->restPPP('/users/'.$userId.'/carts/'.$cartId.'/addresses/billing', $auth, $addressData);
  }

  /**
   * Gets global auto-apply promotions.
   *
   * @return stdClass {code, result}
   */
  public function getAutoApplyPromotions() {
    $auth = array(
      'Authorization: Bearer ' . $this->getClientAccessToken()
    );
    $query = array(
      'type' => 'all'
    );
    return $this->restGet('/promotions', $auth, $query);
  }

  /**
   * Sets cart's delivery country.
   *
   * @param $cartId
   * @param $countryIso
   * @param $userToken
   */
  public function setDeliveryCountry($cartId, $countryIso, $userToken = NULL) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'deliveryCountry' => $countryIso
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/deliveryCountry', $apiAuth['header'], $data, 'PUT');
  }

  /**
   * Sets preview delivery mode. This can be called before shipping address is
   * added to cart, as versus setDeliveryMode() which must be called after.
   *
   * @param $cartId - cart code for authenticated user, cart guid for anonymous user
   * @param $deliveryModeId
   * @param $userToken - null for 'anonymous' user
   *
   * @return stdClass {code, result}
   */
  public function setPreviewDeliveryMode($cartId, $deliveryModeId, $userToken = null) {
    $apiAuth = $this->getApiAuth($userToken);
    $data = array(
      'deliveryModeCode' => $deliveryModeId
    );
    return $this->restPPP('/users/'.$apiAuth['userId'].'/carts/'.$cartId.'/shippingDeliveryMode', $apiAuth['header'], $data, 'PUT');
  }
}
