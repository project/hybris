<?php

/**
 * @file
 * Run Tests for Hybris API.
 */

/**
 * The HybrisAPITest class.
 */
class HybrisAPITest extends PHPUnit_Framework_TestCase {
  /**
   * Test getting product stock level.
   *
   * @group ecom
   */
  public function testGetStockLevel() {
    $hybris_api = hybris_api_get_connector();

    // Should return integer for valid sku IB184.
    $stock = $hybris_api->getStockLevel('IB184', TRUE);
    $this->assertInternalType('int', $stock);

    // Should return FALSE for a phony sku.
    $stock = $hybris_api->getStockLevel('%^&*#S');
    $this->assertFalse($stock);
  }
}
